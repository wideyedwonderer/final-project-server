import { Routes } from './routes';
import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Devices } from './devices';
import { Logs } from './log';
import { Reports } from './reports';
import { Roles } from './roles';


@Entity()
export class Users {
  @PrimaryGeneratedColumn('uuid')
  @OneToMany(() => Users, (users) => users.adminId)
  @JoinColumn({ name: 'user' })
  public userId: string;

  @OneToMany(() => Routes, (routes) => routes.userId)
  @JoinColumn({ name: 'routes' })
  public routes: Routes[];

  @Column({ type: 'nvarchar', length: 50, nullable: false })
  public firstName: string;

  @Column({ type: 'nvarchar', length: 50 })
  public lastName: string;

  @Column({ type: 'nvarchar', length: 100, nullable: false })
  public email: string;

  @Column({ type: 'nvarchar', length: 300, nullable: false })
  public password: string;

  @Column({ type: 'tinyint', default: false, nullable: false })
  public banned: boolean;

  @ManyToOne(() => Users, (users) => users.userId)
  @JoinColumn({ name: 'adminId' })
  public adminId: string;

  @ManyToOne(() => Roles, (roles) => roles.users, { eager: true })
  @JoinColumn({ name: 'role' })
  public role: Roles;

  @ManyToMany(() => Devices, { eager: true })
  @JoinTable()

  public devices: Devices[];

  @ManyToMany(() => Reports, { eager: true })
  @JoinTable()
  public reports: Reports[];

  @ManyToMany(() => Logs, { eager: true })
  @JoinTable()
  public logs: Logs[];

  
}
