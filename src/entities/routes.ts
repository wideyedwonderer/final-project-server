import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Devices } from './devices';
import { Users } from './users';

@Entity()
export class Routes {
    @PrimaryGeneratedColumn('uuid')
    public routeId: string;

    @ManyToOne(() => Users, (users) => users.routes)
    public userId: string;

    @Column({ type: 'nvarchar', length: 2000 })
    public devices: string;

    @Column('nvarchar')
    public name: string;

}
