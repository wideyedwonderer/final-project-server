import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Users } from './users';

@Entity()
export class Reports {
    @PrimaryGeneratedColumn('uuid')
    public reportId: string;

    @Column('nvarchar')
    public name: string;

    @Column('nvarchar')
    public startDate: string;

    @Column({ type: 'nvarchar', nullable: true })
    public endDate: string;
}
