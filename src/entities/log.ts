import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Logs {
    @PrimaryGeneratedColumn('uuid')
    public logId: string;

    @Column('nvarchar')
    public type: string;

    @Column('nvarchar')
    public date: string;

    @Column('nvarchar')
    public description: string;
}
