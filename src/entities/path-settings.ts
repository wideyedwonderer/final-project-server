import { Column, Entity, PrimaryColumn } from 'typeorm';
@Entity()
export class PathSettings {
    @PrimaryColumn()
    public pathId: string;

    @Column('int', {nullable: true})
    public min: number;

    @Column('int', {nullable: true})
    public max: number;
}
