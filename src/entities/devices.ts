import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Devices {
    @PrimaryGeneratedColumn('uuid')
    public deviceId: string;

    @Column('nvarchar')
    public name: string;

    @Column('float')
    public lat: number;

    @Column('float')
    public lng: number;

}
