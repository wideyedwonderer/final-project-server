import {
  Body,
  Controller,
  Get,
  Post,
  Res,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { Roles } from '../decorators/roles.decorator';
import { RolesGuard } from './../guards/roles.guard';
import { AdminService } from './admin.service';

@Roles('admin')
@UseGuards(AuthGuard(), RolesGuard)
@Controller('admin')
export class AdminController {
  public constructor(private readonly adminService: AdminService) {}

}
