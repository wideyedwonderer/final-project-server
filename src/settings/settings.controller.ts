import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { SettingsService } from './settings.service';

@Controller('settings')
export class SettingsController {
  public constructor(private readonly settingsService: SettingsService) {}
  @Get()
   public async getPathSetting(@Query() query: any): Promise<string> {
     return this.settingsService.getPathSetting(query.pathId, query.setting);
   }
  @Post(':id')
   public async setPathMinOrMax(@Param('id') id: string, @Body() body: any): Promise<void> {
     return this.settingsService.setPathSetting(id, body.setting, body.value);
   }
  @Post()
  public async getPathsSettings(@Body() body: string[]): Promise<any[]> {
    let result = Array.from({ length: body.length }, _ => null);
    result = result.map(async(x, i) => this.settingsService.getPathSettings(body[i]));
    const final = Promise.all(result);
    return final;
  }
}
