import { Injectable } from '@nestjs/common/decorators/core/component.decorator';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PathSettings } from '../entities/path-settings';

@Injectable()
export class SettingsService {

    public constructor(
    @InjectRepository(PathSettings) private readonly pathRepository: Repository<PathSettings>) {}

    public async getPathSetting(pathId: string, setting: string): Promise<string> {
        const path = await this.pathRepository.findOne(pathId);
        if (path) {
            return path[setting];
        } else {
            return '';
        }
    }
    public async getPathSettings(pathId: string): Promise<any> {
        const path = await this.pathRepository.findOne(pathId);
        if (path) {
            return path;
        } else {
            this.pathRepository.save({ pathId });
            this.pathRepository.insert({ pathId });
            return { pathId };
        }
    }
    public async setPathSetting(pathId: string, setting: string, value: number): Promise<void> {
        const path = new PathSettings();
        path.pathId = pathId;
        path[setting] = value;
        this.pathRepository.save(path);
        this.pathRepository.insert(path);
    }
}
