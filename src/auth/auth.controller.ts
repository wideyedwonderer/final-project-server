import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../decorators/roles.decorator';
import { UserLoginDTO } from '../users/user-login-dto';
import { RolesGuard } from './../guards/roles.guard';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  public constructor(private readonly authService: AuthService) {}

  @Get()
  @Roles('admin', 'user')
  @UseGuards(AuthGuard(), RolesGuard)
  public root(): string {
    return 'root';
  }

  @Post('login')
  public async login(
    @Body(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    user: UserLoginDTO,
  ): Promise<string> {
    const token = await this.authService.login(user);
    if (!token) {
      throw new BadRequestException('Wrong credentials!');
    }
    return JSON.stringify(token);
  }

  // @Post('register')
  // @UseInterceptors(
  //   FileInterceptor('avatar', {
  //     limits: FileService.fileLimit(1, 2 * 1024 * 1024),
  //     storage: FileService.storage(['public', 'images']),
  //     fileFilter: (req, file, cb) =>
  //       FileService.fileFilter(req, file, cb, '.png', '.jpg'),
  //   }),
  // )
  // async register(
  //   @Body(
  //     new ValidationPipe({
  //       transform: true,
  //       whitelist: true,
  //     }),
  //   )
  //   user: UserRegisterDTO,
  //   @UploadedFile()
  //   file,
  // ): Promise<string> {
  //   const folder = join('.', 'public', 'uploads');
  //   if (!file) {
  //     user.avatarUrl = join(folder, 'default.png');
  //   } else {
  //     user.avatarUrl = join(folder, file.filename);
  //   }

  //   try {
  //     await this.usersService.registerUser(user);
  //     return 'saved';
  //   } catch (error) {
  //     await new Promise((resolve, reject) => {
  //       // Delete the file if user not found
  //       if (file) {
  //         unlink(join('.', file.path), err => {
  //           if (err) {
  //             reject(error.message);
  //           }
  //           resolve();
  //         });
  //       }

  //       resolve();
  //     });

  // tslint:disable-next-line:no-commented-code
  //     return error.message;
  //   }
  // }
}
