import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { GetUserDTO } from '../users/get-user.dto';
import { UsersService } from '../users/users.service';
import { UserLoginDTO } from './../users/user-login-dto';
import { IJwtPayload } from './interfaces/jwt-payload';

@Injectable()
export class AuthService {
  public constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  public async login(user: UserLoginDTO): Promise<string> {
    const userFound: GetUserDTO = await this.usersService.signIn(user);
    if (userFound) {
      return this.jwtService.sign({
        email: userFound.email,
        role: userFound.role.role,
      });
    } else {
      return null;
    }
  }

  public async validateUser(payload: IJwtPayload): Promise<GetUserDTO> {
    return this.usersService.validateUser(payload);
  }
}
