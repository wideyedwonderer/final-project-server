import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as cors from 'cors';
import * as dotenv from 'dotenv';
import { join } from 'path';
import { AppModule } from './app.module';
import { ConfigService } from './config/config.service';

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule);
  dotenv.load();
  app.use(cors());
  app.useStaticAssets(join(__dirname, '..', 'assets'));
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  await app.listen(app.get(ConfigService).port);
};
bootstrap();
