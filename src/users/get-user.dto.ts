
export class GetUserDTO {
  public email: string;

  public password: string;

  public role: {roleId: number, role: string};
}
