
import { IsEmail, IsOptional, IsString, Length, Matches } from 'class-validator';

export class UsersRegistrationDTO {
  @IsString()
  // tslint:disable-next-line:no-magic-numbers
  @Length(2, 50)
  public firstName: string;
  @IsString()
  // tslint:disable-next-line:no-magic-numbers
  @Length(3, 50)
  public lastName: string;
  @IsEmail()
  // tslint:disable-next-line:no-magic-numbers
  @Length(5, 50)
  public email: string;
  @IsString()
  // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  public password: string;

  @IsString()
  @IsOptional()
  public secret: string;

  @IsString()
  @IsOptional()
  public adminId: string;
}
