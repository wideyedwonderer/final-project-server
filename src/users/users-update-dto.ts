import {
  IsBoolean,
  IsEmail,
  IsOptional,
  IsString,
  Length,
  Matches,
} from 'class-validator';

export class UsersUpdateDTO {
  @IsString()
  // tslint:disable-next-line:no-magic-numbers
  @Length(2, 50)
  @IsOptional()
  public firstName: string;
  @IsString()
  // tslint:disable-next-line:no-magic-numbers
  @Length(3, 50)
  @IsOptional()
  public lastName: string;
  @IsEmail()
  // tslint:disable-next-line:no-magic-numbers
  @Length(5, 50)
  @IsOptional()
  public email: string;
  @IsString()
 // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  @IsOptional()
  public password: string;

}
