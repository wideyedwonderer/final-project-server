import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { getManager, Repository } from 'typeorm';
import { IJwtPayload } from '../auth/interfaces/jwt-payload';
import { Roles, Users } from './../entities';
import { GetUserDTO } from './get-user.dto';
import { UserLoginDTO } from './user-login-dto';
import { UsersRegistrationDTO } from './users-register-dto';
import { UsersUpdateDTO } from './users-update-dto';

@Injectable()
export class UsersService {
  public constructor(
                       @InjectRepository(Users) private readonly usersRepository: Repository<Users>) {}
  public async signIn(user: UserLoginDTO): Promise<GetUserDTO> {

    const userFound: any = await getManager().findOne(Users, {
      where: { email: user.email },
    });

    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return userFound;
      }
    }
    return null;
  }
  public async validateUser(payload: IJwtPayload): Promise<GetUserDTO> {
    const userFound: any = await getManager().findOne(Users, {
      where: { email: payload.email },
    });

    return userFound;
  }
  public async switchUserBan(userId: string, adminId: string): Promise<void> {
      const user = await this.usersRepository.findOne(userId);
      if (user.adminId === adminId) {
        user.banned = !user.banned;
      }
  }
  public async findAllUsers(adminId: string): Promise<Users[]> {
    return getManager()
      .createQueryBuilder(Users, 'u')
      .select(['u.firstName', 'u.lastName', 'u.userId', 'u.adminId', 'u.email', 'u.banned'])
      .where('u.adminId = :id', { id: adminId })
      .getMany();
  }

  public async findUser(userEmail: string): Promise<Users> {
    return getManager()
      .createQueryBuilder(Users, 'u')
      .leftJoinAndSelect('u.adminId', 'admin')
      .select(['u.lastName', 'u.firstName', 'u.email', 'u.userId', 'admin.userId', 'u.role'])
      .where('u.email = :email', { email: userEmail })
      .getOne();
  }

  public async addNewUser(user: UsersRegistrationDTO): Promise<string> {
    const isEmailExist = await this.findUser(user.email);
    if (isEmailExist) {
      return `user with ${user.email} already registered`;
    }
    let newRole: string = 'user';
    if (user.secret && user.secret === 'Praskovka') {
      newRole = 'admin';
    }
    return getManager()
      .createQueryBuilder()
      .insert()
      .into(Users)
      .values({
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        // tslint:disable-next-line:no-magic-numbers
        password: await bcrypt.hash(user.password, 10),
        role: await getManager().createQueryBuilder(Roles, 'roles').where('roles.role = :name', { name: newRole }).getOne(),
        adminId: user.adminId,
      })
      .execute().then(() => 'new user successfully added');
  }

  public async updateUserData(
    updatedUser: UsersUpdateDTO,
    userEmail: string,
  ): Promise<string> {
    if (updatedUser.hasOwnProperty('password')) {
      // tslint:disable-next-line:no-magic-numbers
      updatedUser.password = await bcrypt.hash(updatedUser.password, 10);
    }
    const newUser = await getManager()
      .createQueryBuilder()
      .update(Users)
      .set(updatedUser)
      .where('email = :email', { email: userEmail })
      .execute();
    return `successfully updated`;
  }
}
