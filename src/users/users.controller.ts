import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { User } from './user.decorator';
import { UsersRegistrationDTO } from './users-register-dto';
import { UsersUpdateDTO } from './users-update-dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  public constructor(private readonly userService: UsersService) {}
  @Get()
  @UseGuards(AuthGuard())
  public async getAllUsers(@User() user: any, @Res() res: Response): Promise<void> {
    res.send(await this.userService.findAllUsers(user.userId));
  }

  @Post()
  public async createNewAcc(
    @Body()
    user: UsersRegistrationDTO,
  ): Promise<string> {
    return this.userService.addNewUser(user);
  }

  @Get(':email')
  public async getUserById(
    @Param('email') email: string,
    @Res() res: Response,
  ): Promise<void> {
    console.log(await this.userService.findUser(email))
    res.send(
      (await this.userService.findUser(email)) ||
        `no user with email ${email} found`,
    );
  }

  @Post(':email')
  @UseGuards(AuthGuard())
  public async updateUserData(
                              @Param('email') email: string,
                              @User() user: any,
                              @Body() dataToUpdate: UsersUpdateDTO,
                            ): Promise<any> {
    const userRole = user.role.role;

    const cantChange = {
      user: ['email', 'role', 'banned'],
      admin: [],
    };
    const updatedUser: any = {};
    for (const key in dataToUpdate) {
      if (!cantChange[userRole].includes(key)) {
        updatedUser[key] = dataToUpdate[key];
      }
    }
    return this.userService.updateUserData(updatedUser, email);
  }
}
