import { IsString } from 'class-validator';

export class ReportUpdateDto {
    @IsString()
    public endDate: string;

    @IsString()
    public startDate: string;

    @IsString()
    public name: string;

    @IsString()
    public userId: string;
}
