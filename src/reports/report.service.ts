import { Injectable } from '@nestjs/common/decorators/core/component.decorator';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as uuid from 'uuid';
import { Users } from '../entities';
import { Reports } from '../entities/reports';

@Injectable()
export class ReportService {

    public constructor(@InjectRepository(Reports) private readonly reportRepository: Repository<Reports>,
                       @InjectRepository(Users) private readonly usersRepository: Repository<Users>) {}

    public async addReport(name: string, startDate: string, endDate: string, userId: string): Promise<void> {
        const user = await this.usersRepository.findOne(userId);
        const report: Reports = { name, startDate, endDate, reportId: uuid.v4() };
        user.reports.push(report);
        this.reportRepository.save(report);
        this.usersRepository.save(user);
    }
    public async getAllReports(userId: string): Promise<Reports[]> {
        const user = await this.usersRepository.findOneOrFail({ userId });
        return user.reports;
    }

    public async updateOrDeleteReport(reportId: string, options: {}): Promise<any> {
        let report = await this.reportRepository.findOne({ reportId });
        if (Object.keys(options).length > 0) {
            report = { ...report, ...options };
            return this.reportRepository.save(report);
        } else {
            return this.reportRepository.delete(reportId);
        }
    }
}
