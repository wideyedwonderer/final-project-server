import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { Reports } from '../entities/reports';
import { ReportUpdateDto } from './report-update.dto';

import { ReportService } from './report.service';

@Controller('reports')
export class ReportsController {
  public constructor(private readonly reportService: ReportService) {}

  @Get()
  public async getAllReports(@Query() query: any): Promise<Reports[]> {

    return this.reportService.getAllReports(query.userId);
  }

  @Post()
  public async addReport(@Body() report: ReportUpdateDto): Promise<void> {
      return this.reportService.addReport(report.name, report.startDate, report.endDate, report.userId);
  }

  @Post(':id')
  public async updateOrDeleteReport(@Param('id') reportId: string, @Body() options: any): Promise<any> {
      return this.reportService.updateOrDeleteReport(reportId, options);
  }

}
