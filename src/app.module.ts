import { Routes } from './entities/routes';
import { LogsController } from './devices/logs.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdminModule } from './admin/admin.module';
import { AdminService } from './admin/admin.service';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { DeviceService } from './devices/device.service';
import { DevicesController } from './devices/devices.controller';
import { Users } from './entities';
import { Devices } from './entities/devices';
import { Logs } from './entities/log';
import { PathSettings } from './entities/path-settings';
import { Reports } from './entities/reports';
import { ReportsController } from './reports/report.controller';
import { ReportService } from './reports/report.service';
import { SettingsController } from './settings/settings.controller';
import { SettingsService } from './settings/settings.service';
import { UsersModule } from './users/users.module';
import { UsersService } from './users/users.service';
import { RoutesController } from './devices/routes.controller';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    TypeOrmModule.forFeature([Devices, Users, Reports, PathSettings, Logs, Routes]),
    UsersModule,
    ConfigModule,
    AuthModule,
    AdminModule,
  ],
  controllers: [AppController, DevicesController, ReportsController, SettingsController, LogsController, RoutesController],
  providers: [AppService, UsersService, AdminService, DeviceService, ReportService, SettingsService],
})
export class AppModule {}
