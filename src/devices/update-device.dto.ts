import { IsString } from 'class-validator';

export class DeviceUpdateDto {
    @IsString()
    public lat: string;

    @IsString()
    public lng: string;

    @IsString()
    public name: string;
}
