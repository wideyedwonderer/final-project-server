import { Body, Controller, Get, Param, Post, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from './../decorators/roles.decorator';
import { Devices } from './../entities/devices';
import { RolesGuard } from './../guards/roles.guard';
import { AddDeviceDto } from './add-device.dto';
import { DeviceService } from './device.service';

@Controller('devices')
export class DevicesController {
  public constructor(private readonly deviceService: DeviceService) {}

  @Get()
  public async getAllDevices(@Query() query: any): Promise<Devices[]> {
    return this.deviceService.getAllDevices(query.userId);
  }

  @Roles('admin')
  @UseGuards(AuthGuard(), RolesGuard)
  @Post()
  public async addDevice(@Body() device: AddDeviceDto): Promise<void> {
      return this.deviceService.addDevice(device.name, device.lat, device.lng, device.userId);
  }

  @Roles('admin')
  @UseGuards(AuthGuard(), RolesGuard)
  @Post(':id')
  public async updateOrDeleteDevice(@Param('id') deviceId: string, @Body() options: any): Promise<any> {
      return this.deviceService.updateOrDeleteDevice(deviceId, options);
  }

}
