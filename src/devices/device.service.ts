import { Routes } from './../entities/routes';
import { AddLogDto } from './add-log-dto';

import { Injectable } from '@nestjs/common/decorators/core/component.decorator';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import { Repository } from 'typeorm';
import * as uuid from 'uuid';
import { Users } from './../entities';
import { Devices } from './../entities/devices';
import { Logs } from './../entities/log';

@Injectable()
export class DeviceService {

    public constructor(@InjectRepository(Devices) private readonly deviceRepository: Repository<Devices>,
                       @InjectRepository(Users) private readonly usersRepository: Repository<Users>,
                       @InjectRepository(Logs) private readonly logsRepository: Repository<Logs>,
                       @InjectRepository(Routes) private readonly routeRepository: Repository<Routes>) {}

    public async addDevice(name: string, lat: number, lng: number, userId: string): Promise<void> {
        const user = await this.usersRepository.findOne(userId);
        const device: Devices = { name, lat, lng, deviceId: uuid.v4() };
        user.devices.push(device);
        this.deviceRepository.save(device);
        this.usersRepository.save(user);
    }

    public async addLog(log: AddLogDto, userId: string): Promise<void> {
        const user = await this.usersRepository.findOne(userId);
        const logToPush: any = { ...log, date: moment().toString() };
        logToPush.logId = uuid.v4();
        this.logsRepository.create(logToPush);
        user.logs.push(logToPush);
        this.usersRepository.save(user);
        this.logsRepository.save(logToPush);
    }
    public async getAllLogs(userId: string): Promise<any[]> {
        const user = await this.usersRepository.findOne(userId);
        return user.logs;
    }
    public async getAllDevices(userId: string): Promise<Devices[]> {
        const user = await this.usersRepository.findOne(userId);
        return user.devices;
    }

    public async updateOrDeleteDevice(deviceId: string, options: {}): Promise<any> {
        let device = await this.deviceRepository.findOne({ deviceId });
        if (Object.keys(options).length > 0) {
            device = { ...device, ...options };
            return this.deviceRepository.save(device);
        } else {
            return this.deviceRepository.delete(deviceId);
        }
    }

    public async addNewRoute(userId: string, body: any): Promise<any> {
            const route = new Routes();
            route.devices = JSON.stringify(body.devices);
            route.userId = userId;
            route.routeId = uuid.v4();
            route.name = body.name;
            this.routeRepository.create(route);
            this.routeRepository.save(route);
            return JSON.stringify(route);
    }

    public async getAllRoutes(userId: string): Promise<any> {
        return this.routeRepository.find({ userId });
    }

    public async removeRoute(routeId: string): Promise<any> {
        return this.routeRepository.delete(routeId);
    }
}
