import { Body, Controller, Get, Post, UseGuards, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from './../users/user.decorator';
import { DeviceService } from './device.service';

@Controller('routes')
export class RoutesController {
  public constructor(private readonly deviceService: DeviceService) {}
  @UseGuards(AuthGuard())
  @Post(':id')
  public async deleteRoute(@User() user: any, @Param('id') id: string): Promise<void> {
      return this.deviceService.removeRoute(id);
  }
  
  @UseGuards(AuthGuard())
  @Post()
  public async addNewRoute(@User() user: any, @Body() body: any): Promise<void> {
    return this.deviceService.addNewRoute(user.userId, body);
  }

  @UseGuards(AuthGuard())
  @Get()
  public async getAllRoutes(@User() user: any): Promise<void> {
      return this.deviceService.getAllRoutes(user.userId);
  }

  
}