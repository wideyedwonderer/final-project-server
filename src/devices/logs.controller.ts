import { Roles } from './../decorators/roles.decorator';
import { User } from './../users/user.decorator';
import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from './../guards/roles.guard';
import { AddLogDto } from './add-log-dto';
import { DeviceService } from './device.service';

@Roles('admin')
@UseGuards(AuthGuard(), RolesGuard)
@Controller('logs')
export class LogsController {
  public constructor(private readonly deviceService: DeviceService) {}

  @Post()
  public async addLog(@User() user: any, @Body() log: AddLogDto): Promise<void> {
    return this.deviceService.addLog(log, user.userId);

}

  @Get()
  public async getAllLogs(@User() user: any): Promise<any> {
    return this.deviceService.getAllLogs(user.userId);
  }

};