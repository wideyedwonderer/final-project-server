import { IsNumber, IsString } from 'class-validator';

export class AddDeviceDto {
    @IsNumber()
    public lat: number;

    @IsNumber()
    public lng: number;

    @IsString()
    public name: string;

    @IsString()
    public userId: string;
}
