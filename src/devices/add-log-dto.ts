import { IsString } from 'class-validator';

export class AddLogDto {

    @IsString()
    public type: string;

    @IsString()
    public description: string;
}
